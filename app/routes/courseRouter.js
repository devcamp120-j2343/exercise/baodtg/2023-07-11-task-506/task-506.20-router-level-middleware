//Khai báo thư viện expres
const express = require('express');

//khai báo middleware
const {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware,
} = require('../middlewares/couresMiddleware');

//tạo ra router:
const courseRouter = express.Router();
courseRouter.use(getAllCoursesMiddleware);

courseRouter.get('/courses', (req, res)=> {
    res.json({
        message: 'Get all course'
    })
})
courseRouter.get('/courses/:courseId', (req, res)=> {
    let courseId = req.params.courseId
    res.json({
        message: `Get course id = ${courseId}`
    })
})

courseRouter.post('/courses',postCoursesMiddleware, (req, res)=> {
    res.json({
        message: `Create new course`
    })
})

courseRouter.put('/courses/:courseId',putCoursesMiddleware, (req, res)=> {
    let courseId = req.params.courseId
    res.json({
        message: `Update course id = ${courseId}`
    })
})

courseRouter.delete('/courses/:courseId',deleteCoursesMiddleware, (req, res)=> {
    let courseId = req.params.courseId
    res.json({
        message: `Delete course id = ${courseId}`
    })
})


module.exports = {courseRouter}